// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyANPhjQCzRBvi_td06EUeWHbUB3CJhXNM8',
    authDomain: 'massoterapia-ad4cc.firebaseapp.com',
    databaseURL: 'https://massoterapia-ad4cc.firebaseio.com',
    projectId: 'massoterapia-ad4cc',
    storageBucket: 'massoterapia-ad4cc.appspot.com',
    messagingSenderId: '475704489368',
    appId: '1:475704489368:web:1ec874885737cc544be7ed',
    measurementId: 'G-RZ4J5L7JGN'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
