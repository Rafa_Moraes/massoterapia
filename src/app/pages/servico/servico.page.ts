import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servico',
  templateUrl: './servico.page.html',
  styleUrls: ['./servico.page.scss'],
})
export class ServicoPage implements OnInit {
  data: any;

  constructor() { }

  ngOnInit() {
    fetch('./assets/db/servico.json').then(res => res.json())
    .then(json => {
      this.data = json;
    });
  }

}
