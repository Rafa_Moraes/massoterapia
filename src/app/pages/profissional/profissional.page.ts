import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profissional',
  templateUrl: './profissional.page.html',
  styleUrls: ['./profissional.page.scss'],
})
export class ProfissionalPage implements OnInit {
  data: any;

  constructor() { }

  ngOnInit() {
    fetch('./assets/db/profissional.json').then(res => res.json())
    .then(json => {
      this.data = json;
    });
  }

}
